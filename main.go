package main

import (
	"be-6/app/controller"
	"be-6/app/middleware"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func setupServer() *gin.Engine {
	router := gin.Default()

	cfg := cors.DefaultConfig()
	cfg.AllowAllOrigins = true
	cfg.AllowCredentials = true
	cfg.AllowMethods = []string{"GET", "POST"}
	cfg.AllowHeaders = []string{"Authorization", "Origin", "Accept", "X-Requested-With", " Content-Type", "Access-Control-Request-Method", "Access-Control-Request-Headers"}
	router.Use(cors.New(cfg))

	router.POST("/api/v1/user/add", controller.CreateUser)
	router.POST("/api/v1/login", controller.Login)
	router.GET("/api/v1/google/login", controller.LoginGoogle) // login google
	router.GET("/callback", controller.HandleLoginGoogle)      // google callback
	router.GET("/api/v1/users", controller.GetAllUser)
	router.GET("/api/v1/user/detail/", middleware.Auth, controller.GetProfil)
	router.PUT("/api/v1/user/detail/update", middleware.Auth, controller.UpdateProfil)
	router.GET("/api/v1/user/verifikasi/daftar/:verivication", middleware.Auth, controller.GetAllUserVerifikasi)
	router.PUT("/api/v1/user/password/update", middleware.Auth, controller.UpdatePassword)
	router.GET("/api/v1/user/detail/:id", middleware.Auth, controller.GetOtherUser)              //data user lain
	router.GET("/api/v1/user/email/verifikasi/:kode", controller.EmailVerivication)              //data user lain
	router.POST("/api/v1/user/verifikasi/set", middleware.Auth, controller.VerivikasiManualUser) //untuk role admin verivikasi user manual

	// GetAllKategori
	router.GET("/api/v1/kategori/list", controller.GetAllKategori)
	// FollowKategori
	router.POST("/api/v1/kategori/set", middleware.Auth, controller.FollowKategori)
	// GetAllKategoriFollow
	router.GET("/api/v1/kategori/list/follow", middleware.Auth, controller.GetAllKategoriFollow)
	// GetAllKategoriFollowUserLain
	router.GET("/api/v1/kategori/list/follow/:id", controller.GetAllKategoriFollowUserLain)
	// InsertNewKategoriFeed
	router.POST("/api/v1/kategori/add", controller.InsertNewKategoriFeed)
	// UpdateKategoriFeed
	router.PUT("/api/v1/kategori/update", controller.UpdateKategoriFeed)

	// InsertNewFeed POSTa auth /api/v1/feed/add
	router.POST("/api/v1/feed/add", middleware.Auth, controller.InsertNewFeed)
	// GetDetailFeed GET id /api/v1/feed/detail/:id-feed
	router.GET("/api/v1/feed/detail/:id", controller.GetDetailFeed)
	// GetAllFeeds Auth /api/v1/feeds/user
	router.GET("/api/v1/feeds/user", middleware.Auth, controller.GetAllFeeds)
	// GetAllFeeds Auth /api/v1/feeds/user
	router.GET("/api/v1/feeds/user/all/:id", controller.GetAllFeedsUserLain)
	// GetAllFeedsTrending /api/v1/feeds/trending
	router.GET("/api/v1/feeds/trending", controller.GetAllFeedsTrending)


	// GetDetailFeed GET id /api/v1/feed/detail/:id-feed
	router.GET("/api/v1/feed/user/detail/:id", middleware.Auth, controller.GetDetailFeedUserLogin)
	// GetAllFeedsTrending UserLogin /api/v1/feeds/trending
	router.GET("/api/v1/feeds/user/trending", middleware.Auth, controller.GetAllFeedsTrendingUserLogin)


	// GetAllFeedsUser Auth /api/v1/feeds/user
	router.GET("/api/v1/user/feeds", middleware.Auth, controller.GetAllFeedsUser)

	// Like-Dislike
	router.POST("/api/v1/feeds/like", middleware.Auth, controller.LikeDislikeFeed)

	// Create Comment
	router.POST("/api/v1/feeds/comment/add", middleware.Auth, controller.InsertNewComend)

	// Delete Comment
	router.POST("/api/v1/feeds/comment/delete", middleware.Auth, controller.DeleteComend)

	// CreateNewGroup {nama_group, deskripsi_group} | auth
	router.POST("/api/v1/group/add", middleware.Auth, controller.CreateNewGroup)
	// GetAllGroup ()
	router.GET("/api/v1/groups", controller.GetAllGroup)
	// GetDetailGroup /:id
	router.GET("/api/v1/group/detail/:id", controller.GetDetailGroup)
	// GetAllGroupUserLogin () | auth
	router.GET("/api/v1/group/user/login", middleware.Auth, controller.GetAllGroupUserLogin)
	// UpdateGroup {id, nama_group, deskripsi_group} | auth
	router.PUT("/api/v1/group/update", middleware.Auth, controller.UpdateGroup)
	// InviteMember {id_user, id_group, status_admin} | auth
	router.POST("/api/v1/group/invite", middleware.Auth, controller.InviteMember)
	// SetAdmin {id, StatusAdmin}   id adalah id member | auth
	router.POST("/api/v1/group/setadmin", middleware.Auth, controller.SetAdmin)
	// DeleteMember {id} id adalah id member | auth
	router.POST("/api/v1/group/deletemember", middleware.Auth, controller.DeleteMember)
	// DeleteGroup {id} | auth
	router.POST("/api/v1/group/delete/:id", middleware.Auth, controller.DeleteGroup)

	return router
}

func main() {
	setupServer().Run(":80")
}
