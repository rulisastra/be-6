module be-6

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/mitchellh/mapstructure v1.3.3
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20201016220609-9e8e0b390897
	golang.org/x/oauth2 v0.0.0-20201109201403-9fd604954f58
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.6
)
