package model
import (
	// "be-6/app/utils"
	// "github.com/dgrijalva/jwt-go"
	
	"github.com/pkg/errors"
	"gorm.io/gorm"

	// "fmt"
	"time"
	// "sort"
	// "strconv"
)


type Comment struct {
	ID            int  `gorm:"primary_key" json:"id"`
	IdUser      int  `orm:"id_user" json:"id_user"`
	IdFeed      int  `orm:"id_feed" json:"id_feed"`
	IsiKomentar string `orm:"isi_komentar" json:"isi_komentar"`
	Waktu       int64  `orm:"waktu" json:"waktu"`
}

type Json_Comment struct {
	ID            int  `gorm:"primary_key" json:"id"`
	IdUser      int  `json:"id_user"`
	NamaUser      string  `json:"nama_user"`
	Username      string  `json:"username"`
	IdFeed      int  `json:"id_feed"`
	IsiKomentar string `json:"isi_komentar"`
	Waktu       int64  `json:"waktu"`
}



// createComment
func InsertNewComend(comment Comment, id_user int) (bool,error){
	comment.IdUser=id_user
	comment.Waktu = time.Now().Unix()

	if err := DB.Create(&comment).Error;err!=nil{
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}

	err := DB.Transaction(func(tx *gorm.DB) error {
		var feedDB Feed
		if err := tx.Model(&Feed{}).Where(&Feed{ID: comment.IdFeed}).First(&feedDB).
			Updates(Feed{JumlahComment: feedDB.JumlahComment+1}).
			Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}



// delete comment
func DeleteComend(comment Comment) (bool,error){
	if err := DB.Delete(&comment).Error;err!=nil{
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	err := DB.Transaction(func(tx *gorm.DB) error {
		var feedDB Feed
		if err := tx.Model(&Feed{}).Where(&Feed{ID: comment.IdFeed}).First(&feedDB).
		Updates(Feed{JumlahComment: feedDB.JumlahComment-1}).
		Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

// getComent nanti di model feed
func GetCommentFeed(id_feed int) ([]Json_Comment){
	var comment []Comment
	var jsonComment []Json_Comment

	if err := DB.Where("id_feed =(?)", id_feed).Order("waktu desc").Find(&comment).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			return []Json_Comment{}
		} else {
			return []Json_Comment{}
		}
	}

	for _, f := range comment {
		var user Users
		var username string
		var namauser string
		var jl Json_Comment
		if err := DB.Where(&Users{ID: f.IdUser}).First(&user).Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				username = "Anonymous"
				namauser = "Anonymous"
			} else {
				username = user.Username
				namauser = user.NameUser
			}
		}else{
			username = user.Username
			namauser = user.NameUser
		}
		jl.ID = f.ID
		jl.IdUser = f.IdUser
		jl.NamaUser = namauser
		jl.Username = username
		jl.IdFeed = f.IdFeed
		jl.IsiKomentar = f.IsiKomentar
		jl.Waktu = f.Waktu
		jsonComment = append(jsonComment, jl)
	}

	return jsonComment
}