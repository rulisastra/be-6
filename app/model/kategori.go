package model
import (
	// "be-6/app/utils"
	// "github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	"fmt"
)


type Kategori_diikuti struct {
	ID 		  int    `gorm:"primary_key" json:"id"`
	IdKatFeed int `orm:"id_kat_feed" json:"id_kategori"`
	IdUser    int `orm:"id_user" json:"id_user"`
}

type Kategori_feed struct {
	ID 			int    `gorm:"primary_key" json:"id_kategori"`
	NamaKatFeed string `orm:"nama_kat_feed" json:"nama_kategori"`
}

type Json_follow_unfollow struct{
	ID		int `json:"id_kategori"`
	Follow 	bool `json:"follow"` 
}

type Json_kategori_diikuti struct {
	ID 		  int    `gorm:"primary_key" json:"id"`
	IdKatFeed int `orm:"id_kat_feed" json:"id_kategori"`
	IdUser    int `orm:"id_user" json:"id_user"`
	NamaKategoriFeed    string `json:"nama_kategori_feed"`
}



// List Kategori
func GetAllKategori() (bool,error, []Kategori_feed){
	var kat_feed []Kategori_feed
	if err := DB.Find(&kat_feed).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			return false,errors.Errorf("Kategori Feed is not found"),[]Kategori_feed{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Kategori_feed{}
		}
	}

	return true,nil,kat_feed
}

// Follow Unfollow kategori
func FollowKategori(jfu Json_follow_unfollow, id_user int) (bool,error){
	var nilaiCek int64
	var kd Kategori_diikuti
	kd.IdKatFeed = jfu.ID
	kd.IdUser = id_user
	fmt.Println(id_user)
	err :=DB.Model(&kd).Where("id_kat_feed =? AND id_user=?", jfu.ID, id_user).Count(&nilaiCek)	
	if err != nil {
		if(nilaiCek == 0){
			if err := DB.Create(&kd).Error;err!=nil{
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			return true, nil
		}else{
			if err := DB.Where("id_kat_feed = ? AND id_user=?", jfu.ID, id_user).Delete(&kd).Error;err!=nil{
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			return true, nil
		}
	}else{
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
}

// List Kategori Follow
// var a []aclStruct{}
// a = append(a, aclStruct{"A"}, ...)
func GetAllKategoriFollow(id_user int) (bool,error, []Json_kategori_diikuti){
	
	var kat_diikuti []Kategori_diikuti
	fmt.Println(id_user)
	if err := DB.Where("id_user =?", id_user).Find(&kat_diikuti).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			return false,errors.Errorf("Kategori Feed is not found"),[]Json_kategori_diikuti{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Json_kategori_diikuti{}
		}
	}

	// 
	// fmt.Println(id_user)
	var a []Json_kategori_diikuti
	for _, t := range kat_diikuti {
		var kat_feed Kategori_feed
		DB.Where(&Kategori_feed{ID: t.IdKatFeed}).First(&kat_feed)
		a = append(a, Json_kategori_diikuti{ID: t.ID, IdKatFeed: t.IdKatFeed, IdUser: t.IdUser, NamaKategoriFeed: kat_feed.NamaKatFeed})
        // list = append(list, user.UserName)
    }

	return true,nil,a
}

// Admin | buat kategori
func InsertNewKategoriFeed(kategori_feed Kategori_feed) (bool,error){
	var nilaiCek int64
	// fmt.Println(user)
	err :=DB.Model(&kategori_feed).Where("nama_kat_feed =?", kategori_feed.NamaKatFeed).Count(&nilaiCek)
	if err != nil {
		if(nilaiCek == 0){
			if err := DB.Create(&kategori_feed).Error;err!=nil{
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			return true, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
	 	return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}else{
		return false,nil
	}
}



// Admin | edit kategori
func UpdateKategoriFeed(kategori_feed Kategori_feed) (bool,error){
	
	// fmt.Println(user)
	err := DB.Transaction(func(tx *gorm.DB) error {
		var kategori_feedDB Kategori_feed
		if err := tx.Model(&Kategori_feed{}).Where(&Kategori_feed{ID: kategori_feed.ID}).First(&kategori_feedDB).
		Update("nama_kat_feed", kategori_feed.NamaKatFeed).
		Error; err != nil {
			return err
		}
		return nil
	});if err != nil {
		return false, err
	}
	return true,nil
}