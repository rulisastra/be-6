package model
import (
	// "be-6/app/utils"
	// "github.com/dgrijalva/jwt-go"

	"github.com/pkg/errors"
	"gorm.io/gorm"

	"fmt"
	// "time"
	// "sort"
	// "strconv"
)


type Member struct {
	ID          int  `gorm:"primary_key" json:"id"`
	IdUser      int `orm:"id_user" json:"id_user"`
	IdGroup     int `orm:"id_group" json:"id_group"`
	StatusAdmin bool `orm:"status_admin" json:"status_admin"`
}

type Json_Member struct {
	ID          int  `gorm:"primary_key" json:"id"`
	IdUser      int `orm:"id_user" json:"id_user"`
	NamaUser string ` json:"nama_user"`
	Username string ` json:"username"`
	IdGroup     int `orm:"id_group" json:"id_group"`
	StatusAdmin bool `orm:"status_admin" json:"status_admin"`
}


// invite member
func InviteMember(member Member, id_user int) (bool, error) {
	var nilaiCek int64
	var nilaiCek2 int64
	err := DB.Model(&member).Where("id_group =? AND id_user=? And status_admin=?", member.IdGroup, id_user, true).Count(&nilaiCek)
	if err != nil {
		fmt.Println( nilaiCek)
		if nilaiCek == 1 {
			err := DB.Model(&member).Where("id_group =? AND id_user =?", member.IdGroup, member.IdUser).Count(&nilaiCek2)
				if err != nil {
					if nilaiCek2 == 0 {
						if err := DB.Create(&member).Error; err != nil {
							// create member
							return false, errors.Errorf("invalid prepare statement :%+v\n", err)
						}
						return true, errors.Errorf(",invalid prepare statement :%+v\n", err)
					}
					return false, errors.Errorf("invalid prepare statement :%+v\n", err)
				} else {
					return false, nil
				}
		}
		return false, errors.Errorf("Not admin, invalid prepare statement :%+v\n", err)
	
	} else {

		if nilaiCek == 1 {
			err := DB.Model(&member).Where("id_group =? AND id_user =?", member.IdGroup, member.IdUser).Count(&nilaiCek2)
				if err != nil {
					if nilaiCek2 == 0 {
						if err := DB.Create(&member).Error; err != nil {
							// create member
							return false, errors.Errorf("invalid prepare statement :%+v\n", err)
						}
						return true, errors.Errorf("invalid prepare statement :%+v\n", err)
					}
					return false, errors.Errorf("invalid prepare statement :%+v\n", err)
				} else {
					return false, nil
				}
		}



		return false, nil
	}
}


// set admin member
func SetAdmin(member Member, id_user int) (bool, error) {
	var nilaiCek int64
	err := DB.Model(&member).Where("id_group =? AND id_user=? And status_admin=?", member.IdGroup, id_user, true).Count(&nilaiCek)
	if err != nil {
		
		if nilaiCek == 1 {
			err := DB.Transaction(func(tx *gorm.DB) error {
				var memberDB Member
				var status bool
				if member.StatusAdmin {
					status= true
					}else{
						status= false
					}
					fmt.Println(status)
				if err := tx.Model(&Member{}).Where(&Member{ID: member.ID}).First(&memberDB).
				Update("status_admin", status).
					Error; err != nil {
					return err
				}
				return nil
			})
			if err != nil {
				return false, err
			}
			return true, nil
		}
		return false, errors.Errorf("N,invalid prepare statement :%+v\n", err)
	} else {
		if nilaiCek == 1 {
			err := DB.Transaction(func(tx *gorm.DB) error {
				var memberDB Member
				if err := tx.Model(&Member{}).Where(&Member{ID: member.ID}).First(&memberDB).
					Updates(Member{StatusAdmin: member.StatusAdmin}).
					Error; err != nil {
					return err
				}
				return nil
			})
			if err != nil {
				return false, err
			}
			return true, nil
		}
		return false, nil
	}
}

// kick member
func DeleteMember(member Member, id int) (bool, error) {
	var nilaiCek int64
	err := DB.Model(&member).Where("id_group =? AND id_user=? And status_admin=true", member.IdGroup, id).Count(&nilaiCek)
	if err != nil {
		if nilaiCek == 1 {

			err := DB.Transaction(func(tx *gorm.DB) error {
				var memberDB Member
				if err := tx.Model(&Member{}).Where(&Member{ID: member.ID}).Delete(&memberDB).
					Error; err != nil {
					return err
				}
				return nil
			})
			if err != nil {
				return false, err
			}
			return true, nil
	
		}
		return false, errors.Errorf("Not Admin, invalid prepare statement :%+v\n", err)
	} else {
		if nilaiCek == 1 {

			err := DB.Transaction(func(tx *gorm.DB) error {
				var memberDB Member
				if err := tx.Model(&Member{}).Where(&Member{ID: member.ID}).Delete(&memberDB).
					Error; err != nil {
					return err
				}
				return nil
			})
			if err != nil {
				return false, err
			}
			return true, nil
	
			}
		return false, nil
	}

}
// show member (update model group).
func GetMemberGroup(id_group int) ([]Json_Member){
	var member []Member
	var jsonmember []Json_Member

	if err := DB.Where("id_group =(?)", id_group).Find(&member).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			return []Json_Member{}
		} else {
			return []Json_Member{}
		}
	}

	for _, f := range member {
		var user Users
		var username string
		var namauser string
		var jl Json_Member
		if err := DB.Where(&Users{ID: f.IdUser}).First(&user).Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				username = "Anonymous"
				namauser = "Anonymous"
			} else {
				username = user.Username
				namauser = user.NameUser
			}
		}else{
			username = user.Username
			namauser = user.NameUser
		}
		jl.ID = f.ID
		jl.IdUser = f.IdUser
		jl.NamaUser = namauser
		jl.Username = username
		jl.IdGroup =  f.IdGroup
		jl.StatusAdmin =  f.StatusAdmin
		jsonmember = append(jsonmember, jl)
	}

	return jsonmember
}