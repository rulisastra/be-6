package model
import (
	// "be-6/app/utils"
	// "github.com/dgrijalva/jwt-go"
	
	"github.com/pkg/errors"
	"gorm.io/gorm"

	// "fmt"
	"time"
	// "sort"
	// "strconv"
)


type Like struct {
	ID            int  `gorm:"primary_key" json:"id"`
	IdUser int `orm:"id_user" json:"id_user"`
	IdFeed int `orm:"id_feed" json:"id_feed"`
	Waktu  int64 `orm:"waktu" json:"waktu"`
}

type Json_Like struct {
	ID     int  ` json:"id"`
	IdUser int ` json:"id_user"`
	NamaUser string ` json:"nama_user"`
	Username string ` json:"username"`
	IdFeed int ` json:"id_feed"`
	Waktu  int64 ` json:"waktu"`
}

type Json_like_dislike struct{
	ID		int `json:"id_feed"`
	Like 	bool `json:"like"` 
}


func LikeDislikeFeed(ld Json_like_dislike, id_user int) (bool,error){
	var nilaiCek int64
	var like Like
	like.IdUser = id_user
	like.IdFeed = ld.ID
	err :=DB.Model(&like).Where("id_feed =? AND id_user=?", ld.ID, id_user).Count(&nilaiCek)	
	if err != nil {
		if(nilaiCek == 0 && ld.Like == true){
			like.Waktu = time.Now().Unix()
			if err := DB.Create(&like).Error;err!=nil{
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			
			err := DB.Transaction(func(tx *gorm.DB) error {
				var feedDB Feed
				if err := tx.Model(&Feed{}).Where(&Feed{ID: like.IdFeed}).First(&feedDB).
					Updates(Feed{JumlahLiker: feedDB.JumlahLiker+1}).
					Error; err != nil {
					return err
				}
				return nil
			})
			if err != nil {
				return false, err
			}

			return true, nil
		}else{
			if err := DB.Where("id_feed = ? AND id_user=?", ld.ID, id_user).Delete(&like).Error;err!=nil{
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			err := DB.Transaction(func(tx *gorm.DB) error {
				var feedDB Feed
				if err := tx.Model(&Feed{}).Where(&Feed{ID: like.IdFeed}).First(&feedDB).
					Updates(Feed{JumlahLiker: feedDB.JumlahLiker-1}).
					Error; err != nil {
					return err
				}
				return nil
			})
			if err != nil {
				return false, err
			}
			return true, nil
		}
	}else{
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
}


// show liker nanti update di model feed
func GetLikerFeed(id_feed int) ([]Json_Like){
	var like []Like
	var jsonlike []Json_Like

	if err := DB.Where("id_feed =(?)", id_feed).Order("waktu desc").Find(&like).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			return []Json_Like{}
		} else {
			return []Json_Like{}
		}
	}

	for _, f := range like {
		var user Users
		var username string
		var namauser string
		var jl Json_Like
		if err := DB.Where(&Users{ID: f.IdUser}).First(&user).Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				username = "Anonymous"
				namauser = "Anonymous"
			} else {
				username = user.Username
				namauser = user.NameUser
			}
		}else{
			username = user.Username
			namauser = user.NameUser
		}
		jl.ID = f.ID
		jl.IdUser = f.IdUser
		jl.NamaUser = namauser
		jl.Username = username
		jl.IdFeed = f.IdFeed
		jl.Waktu = f.Waktu
		jsonlike = append(jsonlike, jl)
	}

	return jsonlike
}


// // like feed
// func LikeFeed(like Like, id_user int) (bool,error){
// 	like.IdUser=id_user
// 	like.Waktu = time.Now().Unix()

// 	if err := DB.Create(&like).Error;err!=nil{
// 		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
// 	}

// 	err := DB.Transaction(func(tx *gorm.DB) error {
// 		var feedDB Feed
// 		if err := tx.Model(&Feed{}).Where(&Feed{ID: like.IdFeed}).First(&feedDB).
// 			Updates(Feed{JumlahLiker: feedDB.JumlahLiker+1}).
// 			Error; err != nil {
// 			return err
// 		}
// 		return nil
// 	})
// 	if err != nil {
// 		return false, err
// 	}
// 	return true, nil
// }

// // dislike feed
// func DislikeFeed(like Like, id_user int) (bool,error){
// 	like.IdUser=id_user
// 	like.Waktu = time.Now().Unix()

// 	if err := DB.Delete(&like).Error;err!=nil{
// 		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
// 	}

// 	err := DB.Transaction(func(tx *gorm.DB) error {
// 		var feedDB Feed
// 		if err := tx.Model(&Feed{}).Where(&Feed{ID: like.IdFeed}).First(&feedDB).
// 			Updates(Feed{JumlahLiker: feedDB.JumlahLiker-1}).
// 			Error; err != nil {
// 			return err
// 		}
// 		return nil
// 	})
// 	if err != nil {
// 		return false, err
// 	}
// 	return true, nil
// }
