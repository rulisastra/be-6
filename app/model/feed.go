package model
import (
	// "be-6/app/utils"
	// "github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"gorm.io/gorm"
	// "fmt"
	"time"
	"sort"
	// "strconv"
	// "math"
)


type Feed struct {
	ID            int  `gorm:"primary_key" json:"id"`
	Judul         string `orm:"judul" json:"judul"`
	IdKatFeed     int64  `orm:"id_kat_feed" json:"id_kat_feed"`
	IdJenFeed     int64  `orm:"id_jen_feed" json:"id_jen_feed"`
	IsiFeed       string `orm:"isi_feed" json:"isi_feed"`
	LinkVidio     string `orm:"link_vidio" json:"link_vidio"`
	IdProject     int64  `orm:"id_project" json:"id_project"`
	LinkDownload  string `orm:"link_download" json:"link_download"`
	IdUser        int 	 `orm:"id_user" json:"id_user"`
	Waktu         int64  `orm:"waktu" json:"waktu"`
	JumlahLiker   int  `orm:"jumlah_liker" json:"jumlah_liker"`
	JumlahComment int  `orm:"jumlah_comment" json:"jumlah_comment"`
	TredingParam  int64
}

type FeedWithUser struct {
	ID            int  `gorm:"primary_key" json:"id"`
	Judul         string `orm:"judul" json:"judul"`
	IdKatFeed     int64  `orm:"id_kat_feed" json:"id_kat_feed"`
	Kategori		string `json:"nama_kategori"`
	IdJenFeed     int64  `orm:"id_jen_feed" json:"id_jen_feed"`
	IsiFeed       string `orm:"isi_feed" json:"isi_feed"`
	LinkVidio     string `orm:"link_vidio" json:"link_vidio"`
	IdProject     int64  `orm:"id_project" json:"id_project"`
	LinkDownload  string `orm:"link_download" json:"link_download"`
	IdUser        int 	 `orm:"id_user" json:"id_user"`
	Waktu         int64  `orm:"waktu" json:"waktu"`
	JumlahLiker   int  `orm:"jumlah_liker" json:"jumlah_liker"`
	JumlahComment int  `orm:"jumlah_comment" json:"jumlah_comment"`
	TredingParam  int64 `json:"posisi_trending"`
	NameUser	string `json:"name_user"`
	Username	string `json:"username"`
	IsLike	bool `json:"is_like"`
}


// Buat feed baru
func InsertNewFeed(feed Feed, id_user int) (bool,error){
	var nilaiCek int64
	// fmt.Println(id_user)
	feed.IdUser=id_user
	err :=DB.Model(&feed).Where("judul =?", feed.Judul).Count(&nilaiCek)
	if err != nil {
		if(nilaiCek == 0){
			if err := DB.Create(&feed).Error;err!=nil{
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			return true, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
	 	return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}else{
		return false,nil
	}
}

// DetailFeeds
func GetDetailFeed(id_feed int) (bool, error, FeedWithUser, []Json_Like, []Json_Comment) {
	var feed Feed
	if err := DB.Where(&Feed{ID: id_feed}).Find(&feed).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("feed not found"), FeedWithUser{}, []Json_Like{}, []Json_Comment{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), FeedWithUser{},[]Json_Like{}, []Json_Comment{}
		}
	}

	var username string
	var namauser string
	var user Users
	if err := DB.Where(&Users{ID: feed.IdUser}).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			username = "Anonymous"
			namauser = "Anonymous"
		} else {
			username = user.Username
			namauser = user.NameUser
		}
	}else{
		username = user.Username
		namauser = user.NameUser
	}

	return true, nil, FeedWithUser{
		ID            : feed.ID,
		Judul         : feed.Judul,
		IdKatFeed     : feed.IdKatFeed,
		IdJenFeed     : feed.IdJenFeed,
		IsiFeed       : feed.IsiFeed,
		LinkVidio     : feed.LinkVidio,
		IdProject     : feed.IdProject,
		LinkDownload  : feed.LinkDownload,
		IdUser        : feed.IdUser,
		Waktu         : feed.Waktu,
		JumlahLiker   : feed.JumlahLiker,
		JumlahComment : feed.JumlahComment,
		Username : username,
		NameUser : namauser,
	}, GetLikerFeed(feed.ID),GetCommentFeed(feed.ID)
}

// List Feeds
func GetAllFeeds(id_user int) (bool,error, []FeedWithUser){
	var feed []Feed
	follow := false
	var nilaiCek int64
	var kat_diikuti []Kategori_diikuti
	// fmt.Println(id_user)
	if err := DB.Where("id_user =?", id_user).Find(&kat_diikuti).Count(&nilaiCek).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{	
			return false,errors.Errorf("Kategori Feed is not found"),[]FeedWithUser{}
		}
	}
	
	if nilaiCek != 0 {
		follow=true;
	}
	if follow{
		// fmt.Println(kat_diikuti)
		if len(kat_diikuti) != 0 {
			var id_kat_diik []int
			for _, kat_d := range kat_diikuti {
				//  fmt.Println(kat_d.IdKatFeed)
				 id_kat_diik = append(id_kat_diik, kat_d.IdKatFeed)
			}
			// fmt.Println(id_kat_diik)
				if err := DB.Where("id_kat_feed IN (?)", id_kat_diik).Order("waktu desc").Find(&feed).Error;err!=nil{
					if err == gorm.ErrRecordNotFound{
						return false,errors.Errorf("Kategori Feed is not found"), FeedsWithUser(feed, id_user)
					} else {
						return false, errors.Errorf("invalid prepare statement :%+v\n", err), FeedsWithUser(feed, id_user)
					}
				}
				
				return true,nil,FeedsWithUser(feed, id_user)
		}
		return false,errors.Errorf("Jumlah Kategori ikuti tidak ada"),FeedsWithUser(feed, id_user)
	}
	return false,errors.Errorf("Kategori Feed is not found"),FeedsWithUser(feed, id_user)
}

// List Feeds Trending
func GetAllFeedsTrending(id_user_login int) (bool,error, []FeedWithUser){
	// jarak waktu : (jumlah_liker + jumlah_comment)
	waktu_sekarang := time.Now().Unix()
	waktu_enam_hari_lalu := waktu_sekarang-500000
	var feed []Feed
	if err := DB.Where("waktu > ?", waktu_enam_hari_lalu).Order("waktu desc").Find(&feed).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			// return false,errors.Errorf("Kategori Feed is not found"),[]feed{}
			if err2 := DB.Order("waktu desc").Limit(100).Find(&feed).Error;err2!=nil{
				if err == gorm.ErrRecordNotFound{
					return false,errors.Errorf("Kategori Feed is not found"),FeedsWithUser([]Feed{}, id_user_login)
				} else {
					return false, errors.Errorf("invalid prepare statement :%+v\n", err), FeedsWithUser([]Feed{}, id_user_login)
				}			
			}
			feedTreding := FeedsTreding(feed, waktu_sekarang)
			return true,nil,FeedsWithUser(feedTreding, id_user_login)
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), FeedsWithUser([]Feed{}, id_user_login)
		}
	}
	feedTreding := FeedsTreding(feed, waktu_sekarang)
	return true,nil, FeedsWithUser(feedTreding, id_user_login)
}

func FeedsTreding(feed []Feed, waktu int64) ([]Feed){
	// TredingParam
		var feedSalinan []Feed
		for _, f := range feed {
			// lc :=(0)
			lc := int64(f.JumlahLiker + f.JumlahComment)
			jw := waktu - f.Waktu
			if(lc != 0){
				// fmt.Println(jw/ lc)
				f.TredingParam = jw / lc
			}else{
				// fmt.Println(jw)
				f.TredingParam = jw
			}
			feedSalinan = append(feedSalinan, f)
		}
		// feed.sort
		sort.SliceStable(feedSalinan, func(i, j int) bool {
			return feed[i].TredingParam < feed[j].TredingParam
		})
	return feedSalinan
}



// List Feeds
func GetAllFeedsUser(id_user int) (bool,error, []Feed){
	var feed []Feed

	if err := DB.Where("id_user =(?)", id_user).Order("waktu desc").Find(&feed).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			return false,errors.Errorf("Kategori Feed is not found"), []Feed{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Feed{}
		}
	}

	return true,nil,feed
}

func FeedsWithUser(feeds []Feed, id_user_login int) ([]FeedWithUser){
	
	var fwus []FeedWithUser

	for _, f := range feeds {	
		var user Users
		var username string
		var namauser string
		var kategori string
		var fwu FeedWithUser
		if err := DB.Where(&Users{ID: f.IdUser}).First(&user).Error; err != nil {
			if err == gorm.ErrRecordNotFound {
				username = "Anonymous"
				namauser = "Anonymous"
			} else {
				username = user.Username
				namauser = user.NameUser
			}
		}else{
			username = user.Username
			namauser = user.NameUser
		}
		var kategoriF Kategori_feed
		if err2 := DB.Where(&Kategori_feed{ID: int(f.IdKatFeed)}).First(&kategoriF).Error; err2 != nil {
			if err2 == gorm.ErrRecordNotFound {
				kategori = "Anonymous"
			} else {
				kategori = kategoriF.NamaKatFeed
			}
		}else{
			kategori = kategoriF.NamaKatFeed
		}
		var islike bool
		if( id_user_login != 0){
			var like Like
			if err3 := DB.Where("id_feed =? AND id_user=?", f.ID, id_user_login).First(&like).Error; err3 != nil {
				if err3 == gorm.ErrRecordNotFound {
					islike = false
				} else {
					islike = true
				}
			}else{
				islike = true
			}
		}else{
			islike = false
		}
		
		fwu.ID	= f.ID            
		fwu.Judul	= f.Judul         
		fwu.IdKatFeed	= f.IdKatFeed     
		fwu.IdJenFeed	= f.IdJenFeed     
		fwu.IsiFeed	= f.IsiFeed       
		fwu.LinkVidio	= f.LinkVidio     
		fwu.IdProject	= f.IdProject     
		fwu.LinkDownload	= f.LinkDownload  
		fwu.IdUser	= f.IdUser        
		fwu.Waktu	= f.Waktu         
		fwu.JumlahLiker	= f.JumlahLiker   
		fwu.JumlahComment	= f.JumlahComment 
		fwu.TredingParam	= f.TredingParam  
		fwu.NameUser	= namauser	
		fwu.Username	= username	
		fwu.Kategori	= kategori	
		fwu.IsLike	= islike	
		fwus = append(fwus, fwu)
		
	}

	return fwus
	
}