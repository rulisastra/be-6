package model

import (
	// "fmt"
	"be-6/app/utils"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/pkg/errors"
	"gorm.io/gorm"
)

type Users struct {
	ID                 int    `gorm:"primary_key" json:"id"`
	IdUser             int64  `json:"id_user" orm:"id_user"`
	NameUser           string `json:"name" orm:"name_user"`
	IdTypeUser         int64  `json:"type_user" orm:"id_type_user"`
	Username           string `json:"username" orm:"username"`
	Email              string `json:"email" orm:"email"`
	Password           string `json:"password" orm:"password"`
	Birth              int64  `json:"birth" orm:"birth"`
	Gender             string `json:"gender" orm:"gender"` // male/female
	Phone              string `json:"phone" orm:"phone"`
	Address            string `json:"address" orm:"address"`
	PostCode           int64  `json:"postcode" orm:"post_code"`
	ShortBio           string `json:"short_bio" orm:"short_bio"`
	StatusVerification bool   `json:"verification" orm:"status_verification"`
	LoginType          string `json:"login_type" orm:"login_type"`
	KodeEmail          string `json:"kode_email" orm:"kode_email"`
}

type Auth struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type PasswordChange struct {
	PasswordLama string `json:"password_lama"`
	PasswordBaru string `json:"password_baru"`
}

func Login(auth Auth) (bool, error, string) {
	var user Users
	if err := DB.Where(&Users{Email: auth.Email}).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("User not found"), ""
		}
	}

	err := utils.HashComparator([]byte(user.Password), []byte(auth.Password))
	if err != nil {
		return false, errors.Errorf("Incorrect Password"), ""
	} else {

		sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"email":     auth.Email,
			"id":        user.ID,
			"type_user": user.IdTypeUser,
			"username":  user.Username,
		})

		token, err := sign.SignedString([]byte("secret"))
		if err != nil {
			return false, err, ""
		}
		return true, nil, token
	}
}

func LoginGoogle(loginbygoogle Users, pass string) (bool, error, string) {
	//var account User
	result := DB.Where(&Users{Email: loginbygoogle.Email}).First(&loginbygoogle)
	if result.RowsAffected < 1 {
		loginbygoogle.IdTypeUser = 2
		loginbygoogle.StatusVerification = true
		loginbygoogle.LoginType = "By Google"
		loginbygoogle.Password = pass

		if err := DB.Create(&loginbygoogle).Error; err != nil {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), ""
		}

		sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"email":     loginbygoogle.Email,
			"id":        loginbygoogle.ID,
			"type_user": loginbygoogle.IdTypeUser,
			"username":  loginbygoogle.Username,
		})

		// fmt.Println(loginbygoogle)

		token, err := sign.SignedString([]byte("secret"))
		if err != nil {
			return false, err, ""
		}

		return true, nil, token

	}

	sign := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"email":     loginbygoogle.Email,
		"id":        loginbygoogle.ID,
		"type_user": loginbygoogle.IdTypeUser,
		"username":  loginbygoogle.Username,
	})

	token, err := sign.SignedString([]byte("secret"))
	if err != nil {
		return false, err, ""
	}

	return true, nil, token
}

// redirect halaman untuk fe
func RedirectGoogle(w http.ResponseWriter, r *http.Request, url string) {
	http.Redirect(w, r, url, 204)
}

func InsertNewUser(user Users) (bool, error) {
	var nilaiCek int64
	user.IdTypeUser = 2
	user.StatusVerification = false
	user.LoginType = "on website"
	// fmt.Println(user)
	err := DB.Model(&user).Where("email =?", user.Email).Count(&nilaiCek)
	if err != nil {
		if nilaiCek == 0 {
			if err := DB.Create(&user).Error; err != nil {
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			return true, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	} else {
		return false, nil
	}
}

func GetAllUser() (bool, error, []Users) {
	var user []Users
	if err := DB.Find(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("User not found"), []Users{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Users{}
		}
	}

	return true, nil, user
}

func GetProfil(id int) (bool, error, Users) {
	var user Users

	if err := DB.Where(&Users{ID: id}).Find(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("User not found"), Users{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), Users{}
		}
	}

	return true, nil, Users{
		ID:                 user.ID,
		IdUser:             user.IdUser,
		NameUser:           user.NameUser,
		IdTypeUser:         user.IdTypeUser,
		Username:           user.Username,
		Email:              user.Email,
		Birth:              user.Birth,
		Gender:             user.Gender,
		Phone:              user.Phone,
		Address:            user.Address,
		PostCode:           user.PostCode,
		ShortBio:           user.ShortBio,
		StatusVerification: user.StatusVerification,
		LoginType:          user.LoginType,
	}
}

func GetOtherUser(id int) (bool, error, Users) {
	var user Users

	if err := DB.Where(&Users{ID: id}).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("User not found"), Users{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), Users{}
		}
	}

	return true, nil, Users{
		ID:                 user.ID,
		IdUser:             user.IdUser,
		NameUser:           user.NameUser,
		IdTypeUser:         user.IdTypeUser,
		Username:           user.Username,
		Email:              user.Email,
		Birth:              user.Birth,
		Gender:             user.Gender,
		Phone:              user.Phone,
		Address:            user.Address,
		PostCode:           user.PostCode,
		ShortBio:           user.ShortBio,
		StatusVerification: user.StatusVerification,
		LoginType:          user.LoginType,
	}
}

// perbaharui profil, {name, tanggal_lahir, gender, phone, post_code, short_bio  }

func UpdateProfil(user Users, id int) (bool, error) {
	// fmt.Println("ID: ", id)
	// fmt.Println("NameUser: ", user.NameUser)
	// fmt.Println("Birth: ", user.Birth)
	// fmt.Println("Gender: ", user.Gender)
	// fmt.Println("Phone: ", user.Phone)
	// fmt.Println("PostCode: ", user.PostCode)
	// fmt.Println("ShortBio: ", user.ShortBio)

	err := DB.Transaction(func(tx *gorm.DB) error {
		var userDB Users
		if err := tx.Model(&Users{}).Where(&Users{ID: id}).First(&userDB).
			Updates(Users{NameUser: user.NameUser, Birth: user.Birth, Gender: user.Gender, Phone: user.Phone, Address: user.Address, PostCode: user.PostCode, ShortBio: user.ShortBio}).
			Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

func EmailVerivication(kodeEmail string) (bool, error) {
	// fmt.Println("ID: ", id)
	// fmt.Println("NameUser: ", user.NameUser)
	// fmt.Println("Birth: ", user.Birth)
	// fmt.Println("Gender: ", user.Gender)
	// fmt.Println("Phone: ", user.Phone)
	// fmt.Println("PostCode: ", user.PostCode)
	// fmt.Println("ShortBio: ", user.ShortBio)

	err := DB.Transaction(func(tx *gorm.DB) error {
		var userDB Users
		if err := tx.Model(&Users{}).Where(&Users{KodeEmail: kodeEmail}).First(&userDB).
			Update("status_verification", true).
			Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}

func GetAllUserVerifikasi(verivication string) (bool, error, []Users) {
	var userAll []Users
	var tx *gorm.DB
	if verivication == "true" {
		tx = DB.Where("status_verification = ? AND id_type_user != ?", true, 1)
	} else if verivication == "false" {
		tx = DB.Where("status_verification = ? AND id_type_user != ?", false, 1)
	} else {
		tx = DB.Where("id_type_user != ?", 1)
	}

	if err := tx.Find(&userAll).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("User not found"), []Users{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Users{}
		}
	}

	return true, nil, userAll
}

// daftar akun belum verifikasi | GET
// verifikasi manual buat adim

func UpdatePassword(pass PasswordChange, id int) (bool, error) {
	// fmt.Println("Pass Lama: ", pass.PasswordLama)
	// fmt.Println("Pass Baru: ", pass.PasswordBaru)

	var user Users
	if err := DB.Where(&Users{ID: id}).First(&user).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("User not found")
		}
	}

	err := utils.HashComparator([]byte(user.Password), []byte(pass.PasswordLama))
	if err != nil {
		return false, errors.Errorf("Incorrect Password")
	} else {

		err := DB.Transaction(func(tx *gorm.DB) error {
			var userDB Users
			if err := tx.Model(&Users{}).Where(&Users{ID: id}).First(&userDB).
				Update("password", pass.PasswordBaru).
				Error; err != nil {
				return err
			}
			return nil
		})
		if err != nil {
			return false, err
		}
		return true, nil

		// sign := jwt.NewWithClaims(jwt.SigningMethodHS256,jwt.MapClaims{
		// 	"email":auth.Email,
		// 	"id":user.ID,
		// })

		// token ,err := sign.SignedString([]byte("secret"))
		// if err != nil {
		// 	return false,err,""
		// }
		// return true,nil,token
	}

}

func VerivikasiManualUser(user Users, id int) (bool, error) {
	var userCek Users
	if errA := DB.Where(&Users{ID: id, IdTypeUser: 1}).First(&userCek).Error; errA != nil {
		if errA == gorm.ErrRecordNotFound {
			return false, errors.Errorf("Only Admin Type")
		}
	}
	err := DB.Transaction(func(tx *gorm.DB) error {
		var userDB Users
		if err := tx.Model(&Users{}).Where(&Users{ID: user.ID}).First(&userDB).
			Update("status_verification", user.StatusVerification).
			Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}
