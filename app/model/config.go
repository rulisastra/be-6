package model

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func init() {
	var err error
	// DB, err = gorm.Open(mysql.Open(fmt.Sprintf("root:root@tcp(mysql)/db_dgt_mvc?charset=utf8&parseTime=True&loc=Local")), &gorm.Config{}) -- untuk docker
	DB, err = gorm.Open(mysql.Open(fmt.Sprintf("root:root@/db_dgt_mvc?charset=utf8&parseTime=True&loc=Local")), &gorm.Config{})
	// DB, err = gorm.Open(mysql.Open(fmt.Sprintf("root:@/db_dgt_mvc?charset=utf8&parseTime=True&loc=Local")), &gorm.Config{})
	if err != nil {
		panic("failede to connect to database" + err.Error())
	}
	DB.AutoMigrate(new(Users), new(Kategori_diikuti), new(Kategori_feed), new(Feed), new(Like), new(Comment), new(Group), new(Member))

}
