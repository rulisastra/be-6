package model

import (
	"github.com/pkg/errors"
	"gorm.io/gorm"
)


type Group struct {
	ID             int    `gorm:"primary_key" json:"id"`
	NamaGroup      string `orm:"nama_group" json:"nama_group"`
	DeskripsiGroup string `orm:"deskripsi_group" json:"deskripsi_group"`
}

func CreateNewGroup(group Group, id_user int) (bool, error) {
	var nilaiCek int64
	var member Member
	err := DB.Model(&group).Where("nama_group =?", group.NamaGroup).Count(&nilaiCek)
	if err != nil {
		if nilaiCek == 0 {
			if err := DB.Create(&group).Error; err != nil {
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			member.IdUser=id_user
			member.IdGroup= group.ID
			member.StatusAdmin=true
			if err := DB.Create(&member).Error; err != nil {
				return false, errors.Errorf("invalid prepare statement :%+v\n", err)
			}
			return true, errors.Errorf("invalid prepare statement :%+v\n", err)
		}
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	} else {
		return false, nil
	}
}

// get all group
func GetAllGroup() (bool, error, []Group) {
	var group []Group
	if err := DB.Find(&group).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("Group not found"), []Group{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Group{}
		}
	}

	return true, nil, group
}


func GetDetailGroup(id int) (bool, error, Group, []Json_Member) {
	var group Group

	if err := DB.Where(&Group{ID: id}).Find(&group).Error; err != nil {
		if err == gorm.ErrRecordNotFound {
			return false, errors.Errorf("Group not found"), Group{}, []Json_Member{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), Group{},[]Json_Member{}
		}
	}

	// member list
	return true, nil, Group{
		ID:             group.ID,
		NamaGroup:      group.NamaGroup,
		DeskripsiGroup: group.DeskripsiGroup,
	},GetMemberGroup(group.ID)
}


func GetAllGroupUserLogin(id int) (bool, error, []Group) {
	var group []Group
	var member []Member

	if err := DB.Where("id_user =(?)", id).Find(&member).Error;err!=nil{
		if err == gorm.ErrRecordNotFound{
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Group{}
		} else {
			return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Group{}
		}
	}
			var id_g []int
			for _, f := range member {
				id_g = append(id_g, f.IdGroup)
			}

			if err := DB.Where("id IN (?)", id_g).Find(&group).Error;err!=nil{
				if err == gorm.ErrRecordNotFound{
					return false,errors.Errorf("Kategori Feed is not found"), []Group{}
				} else {
					return false, errors.Errorf("invalid prepare statement :%+v\n", err), []Group{}
				}
			}
			return true, nil, group
}

func UpdateGroup(group Group) (bool, error) {

	err := DB.Transaction(func(tx *gorm.DB) error {
		var groupDB Group
		if err := tx.Model(&Group{}).Where(&Group{ID: group.ID}).First(&groupDB).
			Updates(Group{NamaGroup: group.NamaGroup, DeskripsiGroup: group.DeskripsiGroup}).
			Error; err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return false, err
	}
	return true, nil
}


func DeleteGroup(group Group) (bool, error) {
	if err := DB.Where("id = ?", group.ID).Delete(&group).Error;err!=nil{
		return false, errors.Errorf("invalid prepare statement :%+v\n", err)
	}
	return true, nil
}
