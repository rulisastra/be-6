package controller
import (
	"be-6/app/model"
	"be-6/app/utils"
	"github.com/gin-gonic/gin"
	// "log"
	"net/http"
	// "fmt"
	// "time"
	// "strconv"
)


func InsertNewComend(c *gin.Context){
	var comment model.Comment
	id := c.MustGet("id").(int)
	if err := c.Bind(&comment); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	flag,err := model.InsertNewComend(comment, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}

func DeleteComend(c *gin.Context){
	var comment model.Comment
	if err := c.Bind(&comment); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	flag,err := model.DeleteComend(comment); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}