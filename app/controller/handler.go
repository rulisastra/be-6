package controller

import (
	"be-6/app/model"
	"be-6/app/utils"
	"be-6/app/mail"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
	// "fmt"
	"time"
)


func GetAllUser(c *gin.Context){
	log.Println("Get All User")
	flag,err,usr := model.GetAllUser();if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"users":usr,
		},http.StatusOK,"success")
		return
	}
}

func GetOtherUser(c *gin.Context){

	i :=  c.Param("id")
	id, errConv := strconv.Atoi(i)
    if errConv != nil {
        utils.WrapAPIError(c,errConv.Error(),http.StatusInternalServerError)
		return
	}
	
	flag,err,usr := model.GetOtherUser(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"users":usr,
		},http.StatusOK,"success")
		return
	}
}

// Version 2


func CreateUser (c *gin.Context){

	var user model.Users
	if err := c.Bind(&user); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	pass, err := utils.HashGenerator(user.Password); if err != nil{
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	user.Password = pass
	kodeUnik := time.Now().Unix()
	kodeEmail := strconv.FormatInt(kodeUnik, 16)
	user.KodeEmail = kodeEmail
	
	flag,err := model.InsertNewUser(user)
	if flag{
		mail.SendMailVerification( user.Email, "CoCreate Verification Account", user.KodeEmail)
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}


func Login(c *gin.Context){
	var auth model.Auth
	if err := c.Bind(&auth); err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	log.Println("LOGIN")
	flag,err,token := model.Login(auth); if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"token":token,
		},http.StatusOK,"success")
	} else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
	}
}

func GetProfil (c *gin.Context){
	id := c.MustGet("id").(int)
	// fmt.Println(id)
	flag,err,user := model.GetProfil(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"user":user,
		},http.StatusOK,"success")
		return
	}
}

func UpdateProfil (c *gin.Context){
	var user model.Users
	id := c.MustGet("id").(int)
	if err := c.Bind(&user); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.UpdateProfil(user, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}


func EmailVerivication (c *gin.Context){
	kodeEmail := c.Param("kode")
	flag,err := model.EmailVerivication(kodeEmail); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}


func GetAllUserVerifikasi(c *gin.Context){

	verivication :=  c.Param("verivication")
	flag,err,usr := model.GetAllUserVerifikasi(verivication);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"users":usr,
		},http.StatusOK,"success")
		return
	}
}


func UpdatePassword (c *gin.Context){
	var password model.PasswordChange
	var passwordEncrypt model.PasswordChange
	id := c.MustGet("id").(int)
	if err := c.Bind(&password); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	passwordEncrypt.PasswordLama = password.PasswordLama

	passBaru, err := utils.HashGenerator(password.PasswordBaru); if err != nil{
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	passwordEncrypt.PasswordBaru = passBaru

	flag,err := model.UpdatePassword(passwordEncrypt, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}


func VerivikasiManualUser (c *gin.Context){
	var user model.Users
	id := c.MustGet("id").(int)
	if err := c.Bind(&user); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.VerivikasiManualUser(user,id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}