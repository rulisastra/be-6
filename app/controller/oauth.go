package controller

import (
	"be-6/app/model"
	"be-6/app/utils"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	googleOauthConfig = &oauth2.Config{
		ClientID:     "1023361548623-shjml0q72aasj583flq6rs0n6o34t5vp.apps.googleusercontent.com", // cocreate KEL6 FIX
		ClientSecret: "5TAAfdM7p6S_89zblKGFh19V",
		RedirectURL:  "http://kelompok6.dtstakelompok1.com/callback", // sementara
		Scopes: []string{
			"https://www.googleapis.com/auth/userinfo.email",
			"https://www.googleapis.com/auth/userinfo.profile",
		},
		Endpoint: google.Endpoint,
	}
	randomstate = "random"
	state       string
)

func LoginGoogle(c *gin.Context) {

	url := googleOauthConfig.AuthCodeURL(randomstate)
	c.Redirect(http.StatusTemporaryRedirect, url)
}

func HandleLoginGoogle(c *gin.Context) {
	// session := sessions.Default(c)

	// retrievedState := session.Get("state")

	if randomstate != c.Query("state") {

		c.AbortWithError(http.StatusUnauthorized, fmt.Errorf("Invalid session state: %s", randomstate))

		return

	}

	tok, err := googleOauthConfig.Exchange(oauth2.NoContext, c.Query("code"))

	if err != nil {

		c.AbortWithError(http.StatusBadRequest, err)

		return

	}

	client := googleOauthConfig.Client(oauth2.NoContext, tok)
	//https://www.googleapis.com/oauth2/v3/userinfo
	email, err := client.Get("https://www.googleapis.com/oauth2/v3/userinfo")

	if err != nil {

		c.AbortWithError(http.StatusBadRequest, err)

		return

	}

	defer email.Body.Close()

	data, _ := ioutil.ReadAll(email.Body)
	log.Println("Email body: ", string(data))

	// accountModel := model.DB{
	// 	DB: inDB.DB,
	// }

	var accountgoogle model.Users
	json.Unmarshal([]byte(data), &accountgoogle)
	if err := c.Bind(&accountgoogle); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}

	// tambah hash
	pass, err := utils.HashGenerator("123456")
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}

	flag, err, token := model.LoginGoogle(accountgoogle, pass)
	if flag {
		//c.SetCookie("value", token, 3600, "", "", false, true)
		c.Header("Authorization", token)

		c.Header("Refresh", "8")
		c.Redirect(302, "http://kelompok6a.dtstakelompok1.com/google-callback?t="+token) // yey!

		utils.WrapAPIData(c, map[string]interface{}{
			"token": token,
		}, http.StatusOK, "success")
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
	}

	c.Status(http.StatusOK)

	fmt.Println("test")
}
