package controller
import (
	"be-6/app/model"
	"be-6/app/utils"
	"github.com/gin-gonic/gin"
	// "log"
	"net/http"
	// "fmt"
	// "time"
	"strconv"
)


// InsertNewFeed InsertNewFeed(feed Feed, id_user string)
func InsertNewFeed(c *gin.Context){
	var feed model.Feed
	id := c.MustGet("id").(int)
	if err := c.Bind(&feed); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.InsertNewFeed(feed, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}

// GetDetailFeed GetDetailFeed(id_feed int)
func GetDetailFeed(c *gin.Context){
	i :=  c.Param("id")
	id, errConv := strconv.Atoi(i)
    if errConv != nil {
        utils.WrapAPIError(c,errConv.Error(),http.StatusInternalServerError)
		return
	}

	flag,err,list,liker,comment := model.GetDetailFeed(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"Feed":list,
			"liker":liker,
			"comment":comment,
		},http.StatusOK,"success")
		return
	}
}

// GetAllFeeds GetAllFeeds(id_user int)
func GetAllFeeds(c *gin.Context){
	id := c.MustGet("id").(int)

	flag,err,list := model.GetAllFeeds(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"feeds":list,
		},http.StatusOK,"success")
		return
	}
}


// GetAllFeedsTrending GetAllFeedsTrending()
func GetAllFeedsTrending(c *gin.Context){
	id:=0
	flag,err,list := model.GetAllFeedsTrending(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"feeds-trending":list,
		},http.StatusOK,"success")
		return
	}
}




func GetAllFeedsTrendingUserLogin(c *gin.Context){
	id := c.MustGet("id").(int)
	flag,err,list := model.GetAllFeedsTrending(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"feeds-trending":list,
		},http.StatusOK,"success")
		return
	}
}

func GetDetailFeedUserLogin(c *gin.Context){
	id := c.MustGet("id").(int)

	flag,err,list,liker,comment := model.GetDetailFeed(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"Feed":list,
			"liker":liker,
			"comment":comment,
		},http.StatusOK,"success")
		return
	}
}




// GetAllFeedsUser GetAllFeedsUser(id_user int)
func GetAllFeedsUser(c *gin.Context){
	id := c.MustGet("id").(int)

	flag,err,list := model.GetAllFeedsUser(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"feeds":list,
		},http.StatusOK,"success")
		return
	}
}

func GetAllFeedsUserLain(c *gin.Context){
	i :=  c.Param("id")
	id, errConv := strconv.Atoi(i)
	if errConv != nil {
        utils.WrapAPIError(c,errConv.Error(),http.StatusInternalServerError)
		return
	}
	flag,err,list := model.GetAllFeedsUser(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"feeds":list,
		},http.StatusOK,"success")
		return
	}
}