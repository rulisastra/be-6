package controller
import (
	"be-6/app/model"
	"be-6/app/utils"
	"github.com/gin-gonic/gin"
	// "log"
	"net/http"
	// "fmt"
	// "time"
	// "strconv"
)

func LikeDislikeFeed(c *gin.Context){
	var like model.Json_like_dislike
	id := c.MustGet("id").(int)
	if err := c.Bind(&like); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	flag,err := model.LikeDislikeFeed(like, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}

// func LikeFeed(c *gin.Context){
// 	var like model.Like
// 	id := c.MustGet("id").(int)
// 	if err := c.Bind(&like); err!= nil {
// 		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
// 		return
// 	}
// 	flag,err := model.LikeFeed(like, id); if flag{
// 		utils.WrapAPISuccess(c,"success",http.StatusOK)
// 		return
// 	}else {
// 		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
// 		return
// 	}
// }

// func DislikeFeed(c *gin.Context){
// 	var like model.Like
// 	id := c.MustGet("id").(int)
// 	if err := c.Bind(&like); err!= nil {
// 		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
// 		return
// 	}
// 	flag,err := model.DislikeFeed(like, id); if flag{
// 		utils.WrapAPISuccess(c,"success",http.StatusOK)
// 		return
// 	}else {
// 		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
// 		return
// 	}
// }