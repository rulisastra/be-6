package controller
import (
	"be-6/app/model"
	"be-6/app/utils"
	"github.com/gin-gonic/gin"
	// "log"
	"net/http"
	"fmt"
	// "time"
	"strconv"
)


func GetAllKategori (c *gin.Context){
	// GetAllKategori()
	flag,err,kat_feed := model.GetAllKategori();if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"kategori_feed":kat_feed,
		},http.StatusOK,"success")
		return
	}
}

func FollowKategori (c *gin.Context){
	// FollowKategori (jfu Json_follow_unfollow, id_user int)
	var jfu model.Json_follow_unfollow
	fmt.Println(c)
	id := c.MustGet("id").(int)
	if err := c.Bind(&jfu); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	flag,err := model.FollowKategori(jfu, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}

func GetAllKategoriFollow (c *gin.Context){
	// GetAllKategoriFollow(id_user int)
	id := c.MustGet("id").(int)
	flag,err,kf := model.GetAllKategoriFollow(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"kategori_follow":kf,
		},http.StatusOK,"success")
		return
	}

}

func GetAllKategoriFollowUserLain (c *gin.Context){
	// GetAllKategoriFollow(id_user int)
	i :=  c.Param("id")
	id, errConv := strconv.Atoi(i)
	if errConv != nil {
        utils.WrapAPIError(c,errConv.Error(),http.StatusInternalServerError)
		return
	}
	
	flag,err,kf := model.GetAllKategoriFollow(id);if err != nil {
		utils.WrapAPIError(c,err.Error(),http.StatusInternalServerError)
		return
	}
	if flag{
		utils.WrapAPIData(c,map[string]interface{}{
			"kategori_follow":kf,
		},http.StatusOK,"success")
		return
	}

}

func InsertNewKategoriFeed (c *gin.Context){
	// InsertNewKategoriFeed(kategori_feed Kategori_feed)
	var katfeed model.Kategori_feed
	if err := c.Bind(&katfeed); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.InsertNewKategoriFeed(katfeed); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

}

func UpdateKategoriFeed (c *gin.Context){
	// UpdateKategoriFeed(kategori_feed Kategori_feed)
	var kategori_feed model.Kategori_feed
	if err := c.Bind(&kategori_feed); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
	flag,err := model.UpdateKategoriFeed(kategori_feed); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

}