package controller

import (
	"be-6/app/model"
	"be-6/app/utils"
	// "log"
	// "strconv"

	"github.com/gin-gonic/gin"

	// "log"

	"net/http"
	// "time"
)


// InviteMember
func InviteMember(c *gin.Context){
	var member model.Member
	id := c.MustGet("id").(int)
	if err := c.Bind(&member); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.InviteMember(member, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}

// setAdmin
func SetAdmin (c *gin.Context){
	var member model.Member
	id := c.MustGet("id").(int)
	if err := c.Bind(&member); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.SetAdmin(member, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}

// deleteMember
func DeleteMember (c *gin.Context){
	var member model.Member
	id := c.MustGet("id").(int)
	if err := c.Bind(&member); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.DeleteMember(member, id); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}