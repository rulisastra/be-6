package controller

import (
	"be-6/app/model"
	"be-6/app/utils"
	"log"
	"strconv"
	"github.com/gin-gonic/gin"
	"net/http"

)

// CreateNewGroup
func CreateNewGroup(c *gin.Context) {

	var group model.Group
	id := c.MustGet("id").(int)
	if err := c.Bind(&group); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	flag, err := model.CreateNewGroup(group, id)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

// GetAllGroup
func GetAllGroup(c *gin.Context) {
	log.Println("Get All Group")
	flag, err, gr := model.GetAllGroup()
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"group": gr,
		}, http.StatusOK, "success")
		return
	}
}

// GetDetailGroup
func GetDetailGroup(c *gin.Context) {
	i :=  c.Param("id")
	id, errConv := strconv.Atoi(i)
    if errConv != nil {
        utils.WrapAPIError(c,errConv.Error(),http.StatusInternalServerError)
		return
	}
	// fmt.Println(id)
	flag, err, gr, member := model.GetDetailGroup(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"group": gr,
			"member": member,
		}, http.StatusOK, "success")
		return
	}
}


// GetAllGroupUserLogin
func GetAllGroupUserLogin(c *gin.Context) {
	log.Println("Get All Group")
	id := c.MustGet("id").(int)
	flag, err, gr := model.GetAllGroupUserLogin(id)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"group": gr,
		}, http.StatusOK, "success")
		return
	}
}


// UpdateGroup
func UpdateGroup (c *gin.Context){
	var group model.Group

	if err := c.Bind(&group); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.UpdateGroup(group); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}

// DeleteGroup
func DeleteGroup (c *gin.Context){
	var group model.Group
	if err := c.Bind(&group); err!= nil {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}

	flag,err := model.DeleteGroup(group); if flag{
		utils.WrapAPISuccess(c,"success",http.StatusOK)
		return
	}else {
		utils.WrapAPIError(c,err.Error(),http.StatusBadRequest)
		return
	}
}